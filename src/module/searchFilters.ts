import {
  enabledDocumentTypes,
  DocumentMeta,
  isEntity,
  isCompendiumEntity,
  SearchResult,
  packEnabled,
  getCollectionFromType,
} from "./searchLib";
import { setSetting, getSetting } from "./settings";
import { FilterType } from "./store/Filters";
import type {
  SearchFilterConfig,
  SearchFilter,
  WorldSavedFilters,
  ClientSavedFilters,
} from "./store/Filters";
import { ModuleSetting } from "./store/ModuleSettings";

export class SearchFilterCollection {
  disabled: string[] = [];
  dirty = true;

  defaultFilters: SearchFilter[] = [];
  clientFilters: SearchFilter[] = [];
  worldFilters: SearchFilter[] = [];
  combinedFilters: SearchFilter[] = [];

  public get filters(): SearchFilter[] {
    if (this.dirty) {
      this.combinedFilters = [
        ...this.defaultFilters,
        ...this.worldFilters,
        ...this.clientFilters,
      ];
      this.combinedFilters.forEach(
        (f) => (f.disabled = this.disabled.includes(f.id))
      );
      this.dirty = false;
    }
    return this.combinedFilters;
  }

  // Someone changed the filters, will be saved etc.
  filtersChanged(which?: FilterType): void {
    if (which === FilterType.Client) {
      this.saveClient();
    } else if (which === FilterType.World) {
      this.saveWorld();
    } else {
      this.save();
    }
  }

  search(query?: string): SearchFilter[] {
    if (!query) {
      return [...this.filters];
    }
    return this.filters.filter((f) => f.tag.includes(query));
  }

  getFilter(id: string): SearchFilter | undefined {
    return this.filters.find((f) => f.id == id);
  }

  getFilterByTag(tag: string): SearchFilter | undefined {
    return this.filters.filter((f) => !f.disabled).find((f) => f.tag == tag);
  }

  addFilter(filter: SearchFilter): void {
    if (filter.type == FilterType.World) {
      this.worldFilters.push(filter);
      this.filtersChanged(filter.type);
    } else if (filter.type == FilterType.Client) {
      this.clientFilters.push(filter);
      this.filtersChanged(filter.type);
    }
  }

  deleteFilter(id: string): void {
    const f = this.filters.find((f) => f.id === id);
    if (!f) return;
    if (f.type == FilterType.World) {
      const x = this.worldFilters.findIndex((f) => f.id === id);
      if (x != -1) {
        this.worldFilters.splice(x, 1);
      }
    } else if (f.type == FilterType.Client) {
      const x = this.clientFilters.findIndex((f) => f.id === id);
      if (x != -1) {
        this.clientFilters.splice(x, 1);
      }
    }
    this.filtersChanged(f.type);
  }

  resetFilters(): void {
    this.defaultFilters = [];
    this.clientFilters = [];
    this.worldFilters = [];
    this.combinedFilters = [];
    this.dirty = false;
  }

  loadDefaultFilters(): void {
    this.loadCompendiumFilters();
    // this.loadDirectoryFilters();
    this.loadEntityFilters();
    this.dirty = true;
  }

  loadEntityFilters(): void {
    this.defaultFilters = this.defaultFilters.concat(
      enabledDocumentTypes().map((type) => {
        const metadata = DocumentMeta[type];
        return {
          id: metadata.collection,
          type: FilterType.Default,
          tag: metadata.collection,
          subTitle: `${game.i18n.localize(metadata.label)}`,
          filterConfig: {
            folders: "any",
            compendiums: "any",
            entities: [metadata.name],
          },
        };
      })
    );
  }

  loadDirectoryFilters(): void {
    // TODO: find a way to find directories that the user is allowed to see
    if (!game.user?.isGM) return;

    this.defaultFilters = this.defaultFilters.concat(
      enabledDocumentTypes().map((type) => {
        const metadata = DocumentMeta[type];
        return {
          id: `dir.${metadata.collection}`,
          type: FilterType.Default,
          tag: `dir.${metadata.collection}`,
          subTitle: getCollectionFromType(type).directory?.title,
          filterConfig: {
            folders: "any",
            compendiums: [],
            entities: [metadata.name],
          },
        };
      })
    );
  }

  loadCompendiumFilters(): void {
    if (!game.packs) return;
    this.defaultFilters = this.defaultFilters.concat(
      game.packs.filter(packEnabled).map((pack) => {
        return {
          id: pack.collection,
          type: FilterType.Default,
          tag: pack.collection,
          subTitle: pack.metadata.label,
          filterConfig: {
            folders: [],
            compendiums: [pack.collection],
            entities: "any",
          },
        };
      })
    );
  }

  loadClientSave(): void {
    const clientSave: ClientSavedFilters = getSetting(
      ModuleSetting.FILTERS_CLIENT
    );
    this.disabled = clientSave.disabled || [];
    this.clientFilters = clientSave.filters || [];
    this.dirty = true;
  }

  loadWorldSave(): void {
    const worldSave: WorldSavedFilters = getSetting(
      ModuleSetting.FILTERS_WORLD
    );
    this.worldFilters = worldSave.filters || [];
    this.dirty = true;
  }

  loadSave(): void {
    this.loadClientSave();
    this.loadWorldSave();
    Hooks.call("QuickInsert:FiltersUpdated");
  }

  saveWorld(): void {
    if (!game.user?.isGM) return;

    const worldSave: WorldSavedFilters = {
      filters: [],
    };

    for (const filter of this.worldFilters) {
      delete filter.disabled;
      worldSave.filters.push(filter);
    }

    setSetting(ModuleSetting.FILTERS_WORLD, worldSave);
  }

  saveClient(): void {
    const clientSave: ClientSavedFilters = {
      disabled: [],
      filters: [],
    };

    for (const filter of [
      ...this.defaultFilters,
      ...this.worldFilters,
      ...this.clientFilters,
    ]) {
      if (filter.disabled) {
        clientSave.disabled.push(filter.id);
      }
      if (filter.type === FilterType.Client) {
        clientSave.filters.push(filter);
      }
    }
    setSetting(ModuleSetting.FILTERS_CLIENT, clientSave);
  }

  save(): void {
    this.saveClient();
    this.saveWorld();
  }
}

// Is parentFolder inside targetFolder?
function isInFolder(
  parentFolder: string | undefined | null,
  targetFolder: string
): boolean {
  while (parentFolder) {
    if (parentFolder === targetFolder) return true;
    //@ts-expect-error "parent" migrated to "folder"
    parentFolder = game.folders?.get(parentFolder)?.folder;
  }
  return false;
}

export function matchFilterConfig(
  config: SearchFilterConfig,
  item: SearchResult
): boolean {
  let folderMatch = false;
  let compendiumMatch = false;
  let entityMatch = true;

  if (isEntity(item.item)) {
    if (config.folders === "any") {
      folderMatch = true;
    } else {
      for (const f of config.folders) {
        if (isInFolder(item.item.folder?.id, f)) {
          folderMatch = true;
          break;
        }
      }
    }
  } else if (isCompendiumEntity(item.item)) {
    if (config.compendiums == "any") {
      compendiumMatch = true;
    } else {
      compendiumMatch = config.compendiums.includes(item.item.package);
    }
  }

  if (config.entities == "any") {
    entityMatch = true;
  } else {
    entityMatch = config.entities.includes(item.item.documentType);
  }

  return (folderMatch || compendiumMatch) && entityMatch;
}
