import { registerSetting } from "../settings";
import type { IndexingDisabledSetting } from "./IndexingDisabledType";
import { ModuleSetting, SAVE_SETTINGS_REVISION } from "./ModuleSettings";

const moduleSettings = {
  [ModuleSetting.GM_ONLY]: {
    setting: ModuleSetting.GM_ONLY,
    name: "QUICKINSERT.SettingsGmOnly",
    hint: "QUICKINSERT.SettingsGmOnlyHint",
    type: Boolean,
    default: false,
    scope: "world",
  },
  [ModuleSetting.FILTERS_SHEETS_ENABLED]: {
    setting: ModuleSetting.FILTERS_SHEETS_ENABLED,
    name: "QUICKINSERT.SettingsFiltersSheetsEnabled",
    hint: "QUICKINSERT.SettingsFiltersSheetsEnabledHint",
    type: Boolean,
    default: true,
    scope: "world",
  },
  [ModuleSetting.AUTOMATIC_INDEXING]: {
    setting: ModuleSetting.AUTOMATIC_INDEXING,
    name: "QUICKINSERT.SettingsAutomaticIndexing",
    hint: "QUICKINSERT.SettingsAutomaticIndexingHint",
    type: Number,
    choices: {
      3000: "QUICKINSERT.SettingsAutomaticIndexing3s",
      5000: "QUICKINSERT.SettingsAutomaticIndexing5s",
      10000: "QUICKINSERT.SettingsAutomaticIndexing10s",
      "-1": "QUICKINSERT.SettingsAutomaticIndexingOnFirstOpen",
    },
    default: -1,
    scope: "client",
  },
  [ModuleSetting.INDEX_TIMEOUT]: {
    setting: ModuleSetting.INDEX_TIMEOUT,
    name: "QUICKINSERT.SettingsIndexTimeout",
    hint: "QUICKINSERT.SettingsIndexTimeoutHint",
    type: Number,
    choices: {
      1500: "QUICKINSERT.SettingsIndexTimeout1_5s",
      3000: "QUICKINSERT.SettingsIndexTimeout3s",
      7000: "QUICKINSERT.SettingsIndexTimeout7s",
      9500: "QUICKINSERT.SettingsIndexTimeou9_5s",
    },
    default: 1500,
    scope: "world",
  },
  [ModuleSetting.SEARCH_BUTTON]: {
    setting: ModuleSetting.SEARCH_BUTTON,
    name: "QUICKINSERT.SettingsSearchButton",
    hint: "QUICKINSERT.SettingsSearchButtonHint",
    type: Boolean,
    default: false,
    scope: "client",
  },
  [ModuleSetting.ENABLE_GLOBAL_CONTEXT]: {
    setting: ModuleSetting.ENABLE_GLOBAL_CONTEXT,
    name: "QUICKINSERT.SettingsEnableGlobalContext",
    hint: "QUICKINSERT.SettingsEnableGlobalContextHint",
    type: Boolean,
    default: true,
  },
  [ModuleSetting.DEFAULT_ACTION_SCENE]: {
    setting: ModuleSetting.DEFAULT_ACTION_SCENE,
    name: "QUICKINSERT.SettingsDefaultActionScene",
    hint: "QUICKINSERT.SettingsDefaultActionSceneHint",
    type: String,
    choices: {
      show: "SCENES.Configure",
      viewScene: "SCENES.View",
      activateScene: "SCENES.Activate",
    },
    default: "show",
  },
  [ModuleSetting.DEFAULT_ACTION_ROLL_TABLE]: {
    setting: ModuleSetting.DEFAULT_ACTION_ROLL_TABLE,
    name: "QUICKINSERT.SettingsDefaultActionRollTable",
    hint: "QUICKINSERT.SettingsDefaultActionRollTableHint",
    type: String,
    choices: {
      show: "QUICKINSERT.ActionEdit",
      roll: "TABLE.Roll",
    },
    default: "show",
  },
  [ModuleSetting.DEFAULT_ACTION_MACRO]: {
    setting: ModuleSetting.DEFAULT_ACTION_MACRO,
    name: "QUICKINSERT.SettingsDefaultActionMacro",
    hint: "QUICKINSERT.SettingsDefaultActionMacroHint",
    type: String,
    choices: {
      show: "QUICKINSERT.ActionEdit",
      execute: "QUICKINSERT.ActionExecute",
    },
    default: "show",
  },
  [ModuleSetting.SEARCH_TOOLTIPS]: {
    setting: ModuleSetting.SEARCH_TOOLTIPS,
    name: "QUICKINSERT.SettingsSearchTooltips",
    hint: "QUICKINSERT.SettingsSearchTooltipsHint",
    type: String,
    choices: {
      off: "QUICKINSERT.SettingsSearchTooltipsValueOff",
      text: "QUICKINSERT.SettingsSearchTooltipsValueText",
      image: "QUICKINSERT.SettingsSearchTooltipsValueImage",
      full: "QUICKINSERT.SettingsSearchTooltipsValueFull",
    },
    default: "text",
  },
  [ModuleSetting.EMBEDDED_INDEXING]: {
    setting: ModuleSetting.EMBEDDED_INDEXING,
    name: "QUICKINSERT.SettingsEmbeddedIndexing",
    hint: "QUICKINSERT.SettingsEmbeddedIndexingHint",
    type: Boolean,
    default: false,
    scope: "world",
  },
  [ModuleSetting.INDEXING_DISABLED]: {
    setting: ModuleSetting.INDEXING_DISABLED,
    name: "Things that have indexing disabled",
    type: Object,
    default: {
      entities: {
        Macro: [1, 2],
        Scene: [1, 2],
        Playlist: [1, 2],
        RollTable: [1, 2],
      },
      packs: {},
    } as IndexingDisabledSetting,
    scope: "world",
    config: false, // Doesn't show up in config
  },
  [ModuleSetting.FILTERS_CLIENT]: {
    setting: ModuleSetting.FILTERS_CLIENT,
    name: "Own filters",
    type: Object,
    default: {
      saveRev: SAVE_SETTINGS_REVISION,
      disabled: [],
      filters: [],
    },
    config: false, // Doesn't show up in config
  },
  [ModuleSetting.FILTERS_WORLD]: {
    setting: ModuleSetting.FILTERS_WORLD,
    name: "World filters",
    type: Object,
    default: {
      saveRev: SAVE_SETTINGS_REVISION,
      filters: [],
    },
    scope: "world",
    config: false, // Doesn't show up in config
  },
  [ModuleSetting.FILTERS_SHEETS]: {
    setting: ModuleSetting.FILTERS_SHEETS,
    name: "Sheet filters",
    type: Object,
    default: {},
    scope: "world",
    config: false, // Doesn't show up in config
  },
};

export function registerSettings(
  callbacks: {
    [setting: string]: (value?: unknown) => unknown;
  } = {}
): void {
  Object.entries(moduleSettings).forEach(([setting, item]) => {
    registerSetting(
      setting,
      (value) => {
        callbacks[item.setting]?.(value);
      },
      item
    );
  });
}
