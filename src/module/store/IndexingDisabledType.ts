export interface IndexingDisabledSetting {
  entities: {
    [key: string]: number[];
  };
  packs: {
    [key: string]: number[];
  };
}
